package br.edu.iftm.pdm.helloworld
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.edu.iftm.pdm.helloworld.model.ClassTask


class Adapter(
    private var taskList : ArrayList<ClassTask>
): RecyclerView.Adapter<Holder>(){

    private var completeListener : OnCompletedChangeListener? = null
    var selectedTasks = SparseBooleanArray()
    private var tempTaskList = taskList

    fun interface OnCompletedChangeListener
    {
        fun onCompleteTask(task: ClassTask)
    }

    //função para criar o view holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder
    {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.view_task_urgent, parent, false)

        return Holder(itemView, this)
    }

    override fun onBindViewHolder(holder: Holder, position: Int)
    {
        holder.bind(this.taskList[position])
    }

    override fun getItemCount(): Int
    {
        return this.taskList.size
    }

    //Função para mostrar apenas as tasks que nao estao feitas
    fun filterCompletedTasks(showNotDone: Boolean)
    {
        if(showNotDone)
        {
            this.taskList = this.taskList.filter{
                !it.getCheck()
            } as ArrayList<ClassTask>
        }
        else
        {
            this.taskList = this.tempTaskList
        }
        this.notifyDataSetChanged()
    }

    fun getOnCompletedChangeListener() : OnCompletedChangeListener?
    {
        return this.completeListener
    }

    fun setOnCompletedChangeListener(completeListener: OnCompletedChangeListener): Adapter
    {
        this.completeListener = completeListener
        return this
    }



}
