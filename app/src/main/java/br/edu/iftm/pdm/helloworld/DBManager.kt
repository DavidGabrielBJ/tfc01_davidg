package br.edu.iftm.pdm.helloworld
import android.content.ContentValues
import android.content.Context
import android.util.Log
import br.edu.iftm.pdm.helloworld.model.ClassTask

class DBManager (context: Context){
    companion object{
        const val WHERE_ID = "${DBSchema.DBTask.ID} = ?"
        val COLS = arrayOf(DBSchema.DBTask.ID,DBSchema.DBTask.MESSAGE,DBSchema.DBTask.URGENT,DBSchema.DBTask.DONE)
        const val ORDER = "${DBSchema.DBTask.TIMESTAMP} DESC"
        //ordenar usando timestamp como referencia
    }
    private val dbAux = DBAux(context)

    //função para inserir no banco de dados
    fun insert(task: ClassTask) : Long
    {
        Log.i("pega","insert")
        val value = ContentValues()
        value.put(DBSchema.DBTask.MESSAGE,task.getMessage())
        value.put(DBSchema.DBTask.URGENT ,task.getUrgency())
        value.put(DBSchema.DBTask.DONE,task.getCheck())

        val db = this.dbAux.writableDatabase
        val id = db.insert(DBSchema.DBTask.TABLENAME, null, value)

        db.close()

        return id
    }

    //função para atualizar banco de dados
    fun updateDoneTask(task: ClassTask)
    {
        Log.i("pega","update")
        val db = this.dbAux.writableDatabase
        val value = ContentValues()

        value.put(DBSchema.DBTask.DONE,task.getCheck())
        db.update(DBSchema.DBTask.TABLENAME, value ,WHERE_ID,arrayOf(task.getID().toString()))

        db.close()
    }

    //função para buscar todas as tasks
    fun getTasks(): ArrayList<ClassTask>
    {
        Log.i("pega","get")
        val tasks : ArrayList<ClassTask> = ArrayList()
        val db = this.dbAux.readableDatabase
        val dbinsert = db.query(DBSchema.DBTask.TABLENAME, COLS, null, null, null, null, ORDER)

        while(dbinsert.moveToNext())
        {
            val id = dbinsert.getLong(dbinsert.getColumnIndexOrThrow(DBSchema.DBTask.ID))
            val message = dbinsert.getString(dbinsert.getColumnIndexOrThrow(DBSchema.DBTask.MESSAGE))
            val urgent = dbinsert.getInt(dbinsert.getColumnIndexOrThrow(DBSchema.DBTask.URGENT )) == 1
            val done = dbinsert.getInt(dbinsert.getColumnIndexOrThrow(DBSchema.DBTask.DONE)) == 1

            val task = ClassTask(id,message,urgent,done)

            tasks.add(task)
        }

        db.close()

        return tasks
    }

}