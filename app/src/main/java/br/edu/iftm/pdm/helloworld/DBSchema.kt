package br.edu.iftm.pdm.helloworld

object DBSchema {
    object DBTask{
        //criar as variaveis
        const val TABLENAME = "tasks"
        const val ID = "t_id"
        const val MESSAGE = "t_message"
        const val URGENT = "t_urgency"
        const val DONE = "t_done"
        const val TIMESTAMP = "t_timestamp"

        //criar a tabela
        fun getCreateTableQuery(): String{
            return """
            CREATE TABLE IF NOT EXISTS $TABLENAME (
                $ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $MESSAGE TEXT NOT NULL,
                $URGENT  INTEGER DEFAULT FALSE,
                $DONE INTEGER DEFAULT FALSE,
                $TIMESTAMP TEXT DEFAULT CURRENT_TIMESTAMP
            );
            """.trimIndent()
        }
    }
}