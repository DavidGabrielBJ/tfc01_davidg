package br.edu.iftm.pdm.helloworld.model

class ClassTask(private var message : String, private val urgency : Boolean, private var doneCheck : Boolean = false) {
    //private var id : Int = 0
    private var id : Long = 0

    constructor(id : Long, message : String, urgency : Boolean, doneCheck : Boolean): this(message,urgency,doneCheck){
        this.id = id
    }

    fun getUrgency(): Boolean {
        return urgency
    }

    fun getMessage(): String {
        return message
    }

    fun getCheck(): Boolean {
        return doneCheck
    }

    fun setCheck(checkStatus: Boolean){
        doneCheck = checkStatus
    }

    fun getID(): Long {
        return id;
    }

    fun setID(itemID: Long){
        id = itemID
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ClassTask

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    /*
    class ClassTask (private var message : String, private val urgent : Boolean, private var Checked: Boolean = false){
    fun getUrgent(): Boolean
    {
        return urgent
    }

    fun getMessage(): String
    {
        return message
    }

    fun getChecked(): Boolean{

        return Checked
    }

    fun setChecked(check: Boolean){
        Checked=check
    }
}


     */

}