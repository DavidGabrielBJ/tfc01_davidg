package br.edu.iftm.pdm.helloworld
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBAux (context: Context) : SQLiteOpenHelper(context, DBNAME, null, DBVERSION){
    companion object{
        private const val DBNAME = "helloworld.db"
        //criar o arquivo helloworld.db
        private const val DBVERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DBSchema.DBTask.getCreateTableQuery())
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }


}