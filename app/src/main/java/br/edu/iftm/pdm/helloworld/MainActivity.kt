package br.edu.iftm.pdm.helloworld

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.edu.iftm.pdm.helloworld.model.ClassTask

class MainActivity : AppCompatActivity() {

    private lateinit var linearlayout: LinearLayout //pegar o layout para programar o checkbox e o lateinit tira os ! e ?(certeza que vai iniciar a variavel depois)
    //==================================================

    private lateinit var switch1: SwitchCompat
    private var etxtInteger: EditText? = null// a ? indica que pode receber null
    private var txtHistory: TextView? = null// variavel para pegar a frase informada pelo usuario
    private lateinit var etxtString: EditText
    private lateinit var notDoneTask: CheckBox
    private lateinit var RecyclerView: RecyclerView
    private lateinit var Adapter: Adapter


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.etxtInteger = findViewById(R.id.etxtInteger)
        this.etxtString = findViewById(R.id.etxtInteger)
        this.switch1 = findViewById(R.id.switch1)
        this.notDoneTask = findViewById(R.id.notDoneTask)
        this.RecyclerView=findViewById(R.id.RecyclerView)
        this.RecyclerView.layoutManager=LinearLayoutManager(this)
        this.RecyclerView.setHasFixedSize(true)
        this.Adapter=Adapter(DAOSingleton.getTasks(this)).setOnCompletedChangeListener {
            val pos= DAOSingleton.update(this, it)
        }

            this.notDoneTask.setOnCheckedChangeListener{_,isChecked -> this.Adapter.filterCompletedTasks(isChecked)

            }
        this.RecyclerView.adapter = Adapter

    }

    //ao apertar o botão
    fun onClickOk (v: View)
    {

        val valueStr : String = this.etxtString.text.toString()

        if(valueStr.isNotEmpty())
        {
            Log.i("pega","a")
            val createdTask = ClassTask(valueStr, this.switch1.isChecked)
            val adicionar = DAOSingleton.add(this, createdTask)
            Log.i("pega",adicionar.toString())
            this.RecyclerView.adapter!!.notifyItemInserted(adicionar)
            Log.i("pega","b")
            this.RecyclerView.scrollToPosition(adicionar)
        }
    }



}

