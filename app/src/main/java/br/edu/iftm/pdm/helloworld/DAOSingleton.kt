package br.edu.iftm.pdm.helloworld
import android.content.Context
import br.edu.iftm.pdm.helloworld.model.ClassTask

object DAOSingleton {
    private lateinit var taskList: ArrayList<ClassTask>
    private lateinit var dbTask: DBManager
    private var i: Int = 0

    //função para inicializar o BD
    private fun initDAO(context: Context){
        if(!::taskList.isInitialized){
            this.dbTask = DBManager(context)
            this.taskList = this.dbTask.getTasks()
        }
    }

    //adicionar uma task
    fun add(context: Context, t: ClassTask) : Int{
        this.initDAO(context)
        taskList.add(0, t)
        t.setID(this.dbTask.insert(t))
        return 0
    }
    /*
    fun add(t: ClassTask): Int{
        t.setID(i++)
        taskList.add(t)
        return taskList.indexOf(t)
    }
*/
    //atualizar quando aperta em aparecer apenas as tasks que estao prontas
    fun update(context: Context,t: ClassTask) : Int{
        this.initDAO(context)
        val pos = taskList.indexOf(t)
        val task = this.taskList[pos]
        task.setCheck(t.getCheck())
        this.dbTask.updateDoneTask(task)
        return pos
    }
   /*
    fun update(t: ClassTask) : Int{
        val pos = taskList.indexOf(t)
        taskList[pos] = t
        return pos
    }
*/

    /*
    fun getTasks(): ArrayList<ClassTask> {
        return taskList;
    }
     */
    fun getTasks(context: Context):ArrayList<ClassTask>{
        this.initDAO(context)
        return  this.taskList
    }

}