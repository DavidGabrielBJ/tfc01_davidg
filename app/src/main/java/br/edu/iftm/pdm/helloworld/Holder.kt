package br.edu.iftm.pdm.helloworld

import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.edu.iftm.pdm.helloworld.model.ClassTask


class Holder (
    itemView: View,
    private val adapter : Adapter
    ): RecyclerView.ViewHolder(itemView)
{

    private val TaskUrgent: View = itemView.findViewById(R.id.view)
    private val textTask: TextView = itemView.findViewById(R.id.textView_task)
    private val checkboxDone : CheckBox = itemView.findViewById(R.id.checkBox_task)
    private lateinit var task: ClassTask

    init{

        this.checkboxDone.setOnCheckedChangeListener{ _, isChecked -> this.task.setCheck(isChecked)
            this.adapter.getOnCompletedChangeListener()?.onCompleteTask(this.task)
        }
    }
    fun bind(task: ClassTask){
        this.task = task
        if(task.getUrgency())
        {
            //vermelho
            this.TaskUrgent.setBackgroundColor(0xFFFC6C6D.toInt())
        /*
        Problemas com a cor: LiveLiteralKt Not found!
        Não é possível mudar a cor
         */
        }
        else
        {
            //verde
            this.TaskUrgent.setBackgroundColor(0xFFCAF994.toInt())
        }

        this.textTask.text = task.getMessage()

        this.checkboxDone.isChecked = task.getCheck()



    }


}